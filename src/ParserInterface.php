<?php

namespace mp92\parser;

interface ParserInterface
{
    /**
     * @param string $url
     * @param string $tag
     * @return array
     */
    public function process($url, $tag);
}