<?php

namespace mp92\parser;

class Parser implements ParserInterface
{

    /**
     * @param string $url
     * @param string $tag
     * @return array
     */
    public function process($url, $tag)
    {
        $html = file_get_contents($url);

        preg_match_all('#<' . $tag . '.*?>(.*?)</' . $tag . '>#', $html, $matches);

        if ($matches && isset($matches[1])) {
            return $matches[1];
        }

        return [];
    }
}